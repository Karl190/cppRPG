# cppRPG

## Sandbox-RPG

We want to crate a game where players can create their own characters, races and
skills from scratch. Most of it will be playable without any coding. But you
will be able to create your own skills via C-Interface. Most of it will be
changeable by Cconf-files or by ingame UI.

At least thats the plan.

## Filestructures

### Skill-Trees

>0:"Fist Attack"&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;//-> root-tree  #:relativ lvl to start of tree<br/>
>5:"Bash~Defend"&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;//-> 1. branch with decision<br/>
>8:"Ruin Game"&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;//-> 2. branch<br/>
>10:"tree1"~"tree2"~"tree3"//-> next tree specialisation<br/>
>0:"1"~"2"|"34"&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;//-> 1. branch<br/>
>1:"5"|"3"~"4"<br/>
>2:"7"|"8"|"9"<br/>
>3:|"6"~"123"~"10"|"etc."<br/>
>10:"Warrior"&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;//-> next tree specialisation<br/>
and so on.

### Cconf-files

for example : classes
>[Warrior]<br/>
>strength=1<br/>
>"constitution"=1<br/>
>skilltree="Warrior"<br/>
>[Mage]<br/>
>intelligence=2<br/>
>[Rouge]<br/>
>dexterity=1<br/>
>initiative=1<br/>
