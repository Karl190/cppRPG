# Buff

## Fields:

### meta
Contains information specific to each quest.
### name
Name of the quest.
### description
Description of the quest.
### BuffType
Indicates how the Buff should be displayed in the UI.
### duration
Total duration in seconds.
### left
Time left in seconds.
### on_start
Function to be called when the buff is applied.
### on_tick
Function to be called every second
### on_start
Function to be called when the buff ends.
### flat
Flat bonus attributes given by this Buff.
### percent
Percentual bonus attributes given by this Buff.