# Character

## Fields:

### character_class
Contains a read only view to the class that makes up the character.
### character_race
Contains a read only view to the race that makes up the character.
### current_hp
The current HP of the character.
### current_energy
The current Energy of the character. Can be interpreted in different ways depending on the class, e.g. Rage for Warriors, Mana for spellcasters etc.
### level
Current level of the character.
### inventory
Contains the inventory of the character.
### equipment
Contains the equipment of the character.
### professions
The set of Professions of the character.
### base_attributes
The base attributes contains only the attributes given by level up, i.e. only the attributes the character would have if they had no buffs and no equipment.
### bonus_attributes
Contains extra attributes given by buffs and equipment.
### current_skills
Contains the skills currently learned by the character.
### questlog
Contains the questlog of the character.
### buffs
Vector of buffs currently applied to the character.

## Functions:

### get_attributes
Calculates and returns the total attributes (all bonuses included).
### get_max_hp
Returns the maximum amount of hp this character has.
### get_max_enery
Returns the maximum amount of energy resource this character has.