# Equipment

## Fields:

### owner
Reference to the character this equipment belongs to.
### armors/weapons
References to the armor and weapon field for iteration.
### head,neck,back,shoulders,chest,finger,legs,feet, main_hand,side_hand
Slots of armor and weapons containing a pair of the item and a modifier ranging from 0 to 100 showing the efficiency of the stats given by this item.

## Functions:

### equip
TODO
### unequip
TODO
### evaluate
TODO
### just_equip
TODO
### test_all_equips
TODO
### get_equip
Returns an item with via a slot argument.
