# Simple Types and aliases

### Point
Contains x and y value of a point in space.
### lvl_type
Type used to represent levels. Should be unsigned.
### attr_type
Type used to represent attributes. Should be signed and not be integral.
### Area
A point plus a radius.
### ArmorType
Enum representing available types of armor.
### WeaponType
Enum representing available types of weapons.
### ArmorSlotType
Enum representing all slots that armor can belong to.
### WeaponSlotType
Enum representing all slots that weapons can belong to.

### skilltree_node
- level: level required to learn the skills on this node.
- skills: skills available on this node.
- next: next nodes


### QuestArg
- self: Usually a reference to Questowner
- target: Reference to target of action
- area: Area of effect
- skill: Skill used
- location: location where event happened
Any of these may be empty if they are not applicable.

### skill_arg
- target: target of skill
- user: user of skill
- targetarea: target of skill for freely targetable skills.
Any of these may be ignored if they are not applicable.