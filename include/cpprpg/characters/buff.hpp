#ifndef CPPRPG_CHARACTERS_BUFF_HPP
#define CPPRPG_CHARACTERS_BUFF_HPP

#include <cpprpg/common/config.hpp>
#include <functional>
#include <any>

enum class BuffType{GOOD, BAD, NEUTRAL};

class Buff
{
	using BuffFunc = std::function<void(Buff&,Character&)>;
	private:
		std::any meta;
		Buff(std::string name, std::string description, BuffType type, size_t duration, BuffFunc on_start, BuffFunc on_tick, BuffFunc on_end):
			name(name), description(description), type(type), duration(duration), on_start(on_start), on_tick(on_tick), on_end(on_end){}
	public:
		std::string name;
		std::string description;
		BuffType type;
		//seconds
		size_t duration;
		size_t left;
		BuffFunc on_start;
		BuffFunc on_tick;
		BuffFunc on_end;

		Attributes flat;
		AttributesPercent percent;


		static Buff Poisoned();
		static Buff EnergizeFlat();
		static Buff EnergizePercent();
};

auto buff_nothing = [](Buff& self, Character& Char){UNUSED(self); UNUSED(Char);};

#endif // CPPRPG_CHARACTERS_BUFF_HPP
