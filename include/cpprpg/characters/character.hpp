#ifndef CPPRPG_CHARACTERS_CHARACTER_HPP
#define CPPRPG_CHARACTERS_CHARACTER_HPP

#include <cpprpg/characters/characterClass.hpp>
#include <cpprpg/characters/characterRace.hpp>
#include <cpprpg/characters/equipment.hpp>
#include <cpprpg/characters/inventory.hpp>
#include <cpprpg/common/config.hpp>
#include <cpprpg/professions/profession.hpp>
#include <cpprpg/tasks/quest.hpp>
#include <vector>

class Character : public Gameobject
{
	public:
	const CharacterClass character_class;
	const CharacterRace character_race;
	attr_type current_hp;
	attr_type current_energy;
	lvl_type level = 0;
	Inventory inventory;
	Equipment equipment;
	std::set<Profession> professions;
	Attributes base_attributes;
	Attributes bonus_attributes;
	std::vector<std::unique_ptr<Skill>> current_skills;
	QuestLog questlog;
	std::vector<Buff> buffs;

	Character(const std::string& racename,
	          const std::string& classname,
	          Point p,
	          model_type m,
	          std::string name = "Nameless") :
	 Gameobject(p, m),
	 character_class(classname),
	 character_race(racename),
	 inventory(*this),
	 equipment(*this),
	 base_attributes(name)
	{
		base_attributes += character_class.attributes + character_race.attributes;
		current_hp = get_max_hp();
		current_energy = get_max_energy();
	}

	Attributes get_attributes()
	{
		return base_attributes + bonus_attributes + equipment.get_attributes();
	}
	attr_type get_max_hp();
	attr_type get_max_energy();
};
#endif // CPPRPG_CHARACTERS_CHARACTER_HPP
