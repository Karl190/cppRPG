#ifndef CPPRPG_CLASSES_CHARACTERCLASS
#define CPPRPG_CLASSES_CHARACTERCLASS

#include <cpprpg/characters/buff.hpp>
#include <cpprpg/common/config.hpp>
#include <cpprpg/common/helper.hpp>
#include <cpprpg/skills/skill.hpp>
#include <cstdlib>
#include <memory>
#include <set>
#include <string>
#include <vector>

#define DEFAULT_STRENGTH_CLASS 10
#define DEFAULT_DEXTERITY_CLASS 10
#define DEFAULT_INTELLIGENCE_CLASS 10
#define DEFAULT_INITATIVE_CLASS 10
#define DEFAULT_CONSTITUTION_CLASS 10
#define DEFAULT_ARMOR_TYPE HEAVY
#define MAX_SKILLS_PER_CLASS 5
#define MAX_SKILLS_PER_LEVEL 5

#define WEAPON_TYPE_FIELD_NAME "Weapon_Type"
#define ARMOR_TYPE_FIELD_NAME "Armor_Type"

class SkillTree
{
	std::vector<skilltree_node> head;
};

class CharacterClass
{
	public:
	Attributes attributes;
	WearableWeight weight;
	std::set<WearableType> wearable_types;
	SkillTree skill_tree;

	CharacterClass(const std::string& classname) :
	 attributes(classname,
	            {DEFAULT_CONSTITUTION_CLASS,
	             DEFAULT_STRENGTH_CLASS,
	             DEFAULT_INTELLIGENCE_CLASS,
	             DEFAULT_DEXTERITY_CLASS,
	             DEFAULT_INITATIVE_CLASS}),
	 weight(WearableWeight::DEFAULT_ARMOR_TYPE)
	{
		if (!class_conf.sections.count(classname))
			throw std::runtime_error("Couldn't find " + classname);
		const auto& section = class_conf.sections[classname];

		attributes.at(AttributeType::CONSTITUTION) +=
		    class_conf.cconf_get_or<cconf_int>(classname, "constitution", 0);
		attributes.at(AttributeType::STRENGTH) +=
		    class_conf.cconf_get_or<cconf_int>(classname, "strength", 0);
		attributes.at(AttributeType::DEXTERITY) +=
		    class_conf.cconf_get_or<cconf_int>(classname, "dexterity", 0);
		attributes.at(AttributeType::INTELLIGENCE) +=
		    class_conf.cconf_get_or<cconf_int>(classname, "intelligence", 0);
		attributes.at(AttributeType::INITIATIVE) +=
		    class_conf.cconf_get_or<cconf_int>(classname, "initiative", 0);

		const auto& str =
		    class_conf.cconf_get_or<cconf_str>(classname, ARMOR_TYPE_FIELD_NAME, "heavy");
		if (auto tmp = parse_wearable_weight(str))
		{
			weight = *tmp;
		}
		else
		{
			throw std::runtime_error("Invalid value for Armor Type field for " + classname);
		}

		UNUSED(section);
		// TODO: parse weapon types
	}
};

#endif // CPPRPG_CLASSES_CHARACTERCLASS
