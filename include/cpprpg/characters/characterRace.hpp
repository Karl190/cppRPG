#ifndef CPPRPG_RACES_BASERACE
#define CPPRPG_RACES_BASERACE

#include <string>
#include <cpprpg/common/config.hpp>

#define DEFAULT_CONSTITUTION_RACE 10
#define DEFAULT_STRENGTH_RACE 10
#define DEFAULT_INTELLIGENCE_RACE 10
#define DEFAULT_DEXTERITY_RACE 10
#define DEFAULT_INITIATIVE_RACE 10

class CharacterRace
{
	public:
		Attributes attributes;

	    CharacterRace(const std::string& racename) :
	     attributes(racename,
	                {DEFAULT_CONSTITUTION_RACE,
	                 DEFAULT_STRENGTH_RACE,
	                 DEFAULT_INTELLIGENCE_RACE,
	                 DEFAULT_DEXTERITY_RACE,
	                 DEFAULT_INITIATIVE_RACE})
	    {
			if(!race_conf.sections.count(racename))
				throw std::runtime_error("Couldn't find " + racename);

		    attributes.at(AttributeType::CONSTITUTION) +=
		        class_conf.cconf_get_or<cconf_int>(racename, "constitution", 0);
		    attributes.at(AttributeType::STRENGTH) +=
		        class_conf.cconf_get_or<cconf_int>(racename, "strength", 0);
		    attributes.at(AttributeType::DEXTERITY) +=
		        class_conf.cconf_get_or<cconf_int>(racename, "dexterity", 0);
		    attributes.at(AttributeType::INTELLIGENCE) +=
		        class_conf.cconf_get_or<cconf_int>(racename, "intelligence", 0);
		    attributes.at(AttributeType::INITIATIVE) +=
		        class_conf.cconf_get_or<cconf_int>(racename, "initiative", 0);
	    }
};

#endif //CPPRPG_RACES_BASERACE
