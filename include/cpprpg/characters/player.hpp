#ifndef CPPRPG_CHARACTERS_PLAYER_HPP
#define CPPRPG_CHARACTERS_PLAYER_HPP

#include <cpprpg/characters/character.hpp>

class Player : public Character
{
	public:
	Player(const std::string& racename,
	       const std::string& classname,
	       std::string name = "Nameless") :
	 Character(racename, classname, Point(1, 1), 'X', name)
	{
	}

	void level_up()
	{
		level++;
		base_attributes += character_class.attributes + character_race.attributes;
	}
};
#endif // CPPRPG_CHARACTERS_PLAYER_HPP