#ifndef CPPRPG_COMMON_ATTRIBUTE_HPP
#define CPPRPG_COMMON_ATTRIBUTE_HPP

#include <cnl/fixed_point.h>
#include <iostream>
#include <string>
#include <valarray>
using lvl_type = size_t;

using attr_type = double;
enum class AttributeType
{
	INTELLIGENCE,
	STRENGTH,
	DEXTERITY,
	INITIATIVE,
	CONSTITUTION
};

struct Attributes
{
	std::string name;
	std::valarray<attr_type> arr;

	Attributes(std::string name = "", decltype(arr) arr = {0, 0, 0, 0, 0}) : name(name), arr(arr) {}

	attr_type& at(AttributeType type)
	{
		switch (type)
		{
		case AttributeType::INTELLIGENCE:
			return arr[0];
			break;
		case AttributeType::STRENGTH:
			return arr[1];
			break;
		case AttributeType::DEXTERITY:
			return arr[2];
			break;
		case AttributeType::INITIATIVE:
			return arr[3];
			break;
		case AttributeType::CONSTITUTION:
			return arr[4];
			break;
		}
		throw std::logic_error("Out of Enum");
	}

	// overload for attributes BEGIN
	Attributes& operator+=(const Attributes& rhs)
	{
		arr += rhs.arr;
		return *this;
	}

	Attributes operator+(const Attributes& rhs) const { return Attributes(*this) += rhs; }

	Attributes operator-=(const Attributes& rhs)
	{
		arr -= rhs.arr;
		return *this;
	}

	Attributes operator-(const Attributes& rhs) const { return Attributes(*this) -= rhs; }

	bool operator>=(const Attributes& rhs)
	{
		auto ret = arr >= rhs.arr;
		for (size_t i = 0; i < ret.size(); i++)
		{
			if (ret[i] == false)
				return false;
		}
		return true;
	}
	// overload for attributes END

	// overload for attr_type BEGIN
	Attributes& operator/=(const attr_type& rhs)
	{
		arr /= rhs;
		return *this;
	}

	Attributes operator/(const attr_type& rhs) const { return Attributes(*this) /= rhs; }

	Attributes& operator*=(const attr_type& rhs)
	{
		arr *= rhs;
		return *this;
	}

	Attributes operator*(const attr_type& rhs) const { return Attributes(*this) *= rhs; }

	// overload for attr_type END

	// calculates the sum of the differences between this.arr and rhs.arr, if any difference is
	// smaller than 0, it is set to 0
	attr_type eval_diff(const Attributes& rhs)
	    const // Normally used on an items attr_requirements to compare to user
	{
		Attributes diff = *this - rhs;
		diff.arr = diff.arr.apply([](double d) { return d < 0 ? 0 : d; });
		return diff.arr.sum();
	}
};

struct AttributesPercent
{
	attr_type constitution = 0;
	attr_type strength = 0;
	attr_type intelligence = 0;
	attr_type dexterity = 0;
	attr_type initiative = 0;
};
#endif // CPPRPG_COMMON_ATTRIBUTE_HPP
