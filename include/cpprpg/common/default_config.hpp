#ifndef CPPRPG_COMMON_DEFAULT_CONFIG_HPP
#define CPPRPG_COMMON_DEFAULT_CONFIG_HPP
#include <string>

inline std::string default_classes =
R"([Warrior]
strength=1
constitution=1
skilltree="Warrior"
[Mage]
intelligence=2
[Rouge]
dexterity=1
initiative=1
)";

inline std::string default_professions =
R"([Hunter]
skill="hunting"
recipelist="BigGame"
[Chef]
recipelist="Meals"
)";

inline std::string default_races =
R"([Human]
intelligence=2
[Elf]
dexterity=2
[Orc]
strength=2
)";

inline std::string default_recipes =
R"([Bread]
"Flour"=12312
)";

inline std::string warrior_sktree =
R"(0:"Hit"
5:"Bash"
10:"Defend"~"Taunt"
15:"Hit"~"Hit"|"Bash"
20:||"Defend"
)";
#endif //CPPRPG_COMMON_DEFAULT_CONFIG_HPP
