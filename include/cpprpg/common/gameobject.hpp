#ifndef CPPRPG_COMMON_GAMEOBJECT_HPP
#define CPPRPG_COMMON_GAMEOBJECT_HPP

#include <cpprpg/common/config.hpp>

struct Point
{
	size_t x, y;
	Point(size_t x, size_t y) : x(x), y(y) {}
};

using model_type = std::variant<char>;

class Gameobject
{
	public:
	std::optional<model_type> model;
	std::optional<Point> position;
	Gameobject(Point p, model_type m) : model(m), position(p) {}
	virtual ~Gameobject() {}
};

#endif // CPPRPG_COMMON_GAMEOBJECT_HPP
