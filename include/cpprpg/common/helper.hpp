#ifndef CPPRPG_COMMON_HELPER_HPP
#define CPPRPG_COMMON_HELPER_HPP

#include <cpprpg/common/config.hpp>

/// merges m1 into m2, overrides values in m2
template <typename k, typename v> inline void merge_maps(std::map<k, v> m1, std::map<k, v> m2)
{
	for (auto& [key, value] : m1)
	{
		m2[key] = value;
	}
}
/// case-insensitive strcmp
inline bool iequals(const std::string& a, const std::string& b)
{
	if (b.size() != a.size())
		return false;
	for (size_t i = 0; i < a.size(); ++i)
		if (tolower(a[i]) != tolower(b[i]))
			return false;
	return true;
}
/// recieves a string and returns an enum, used for parsing config files
inline std::optional<WearableWeight> parse_wearable_weight(const std::string& str)
{
	if (iequals(str, "light"))
	{
		return WearableWeight::LIGHT;
	}
	else if (iequals(str, "medium"))
	{
		return WearableWeight::MEDIUM;
	}
	else if (iequals(str, "heavy"))
	{
		return WearableWeight::HEAVY;
	}
	else
	{
		return std::nullopt;
	}
}
/// recieves a string and returns an enum, used for parsing config files
inline std::optional<WearableType> parse_wearable_type(const std::string& str)
{
	if (iequals(str, "staff1h"))
	{
		return WearableType::STAFF1H;
	}
	else if (iequals(str, "staff2h"))
	{
		return WearableType::STAFF2H;
	}
	else if (iequals(str, "axe1h"))
	{
		return WearableType::AXE1H;
	}
	else if (iequals(str, "axe2h"))
	{
		return WearableType::AXE2H;
	}
	else if (iequals(str, "bow"))
	{
		return WearableType::BOW;
	}
	else if (iequals(str, "crossbow"))
	{
		return WearableType::CROSSBOW;
	}
	else if (iequals(str, "dagger"))
	{
		return WearableType::DAGGER;
	}
	else if (iequals(str, "mace"))
	{
		return WearableType::MACE;
	}
	else if (iequals(str, "sword1h"))
	{
		return WearableType::SWORD1H;
	}
	else if (iequals(str, "sword2h"))
	{
		return WearableType::SWORD2H;
	}
	else if (iequals(str, "shield"))
	{
		return WearableType::SHIELD;
	}
	else
	{
		return std::nullopt;
	}
}

#endif // CPPRPG_COMMON_HELPER_HPP
