#ifndef CPPRPG_ITEMS_ITEM_HPP
#define CPPRPG_ITEMS_ITEM_HPP

#include <cpprpg/common/config.hpp>
#include <cpprpg/skills/skill.hpp>

class Wearable
{
	public:
	WearableType type;
	WearableWeight weight;
	bool use_requirements_test(Character& user) const;
	Wearable(WearableType t, WearableWeight w) : type(t), weight(w) {}
};

class Item
{
	public:
	Attributes attr;
	unsigned mass;
	std::optional<Skill> use;
	lvl_type lvl_requirement;
	Attributes attr_requirements;
	std::optional<Wearable> wearable;
	/// tests if the skill can used (for wearables: also tests if it can be equipped)
	bool use_requirements_test(Character& user) const;
	Item(Attributes a,
	     unsigned m,
	     std::optional<Skill> u,
	     lvl_type lr = 0,
	     Attributes ar = {},
	     std::optional<Wearable> wearable = std::nullopt) :
	 attr(a),
	 mass(m),
	 use(u),
	 lvl_requirement(lr),
	 attr_requirements(ar),
	 wearable(wearable)
	{
	}
};

#endif // CPPRPG_ITEMS_ITEM_HPP
