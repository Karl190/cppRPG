#ifndef CPPRPG_PROFESSIONS_RECIPE_HPP
#define CPPRPG_PROFESSIONS_RECIPE_HPP


#include <string>
#include <vector>
#include <cpprpg/items/item.hpp>

struct Recipe
{
	std::string name;
	std::vector<std::tuple<Item, size_t>> ingredients;
};

std::map<std::string, Recipe> init_recipes(std::string filename);

#endif // CPPRPG_PROFESSIONS_RECIPE_HPP