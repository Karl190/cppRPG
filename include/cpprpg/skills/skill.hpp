#ifndef CPPRPG_CLASSES_SKILLS
#define CPPRPG_CLASSES_SKILLS

#include <cpprpg/common/config.hpp>

class Character;

struct skill_arg
{
	Character& user;
	Gameobject* target;
	std::optional<Point> targetarea;
	skill_arg(Character& u, Gameobject* t = NULL, std::optional<Point> p = std::nullopt) :
	 user(u),
	 target(t),
	 targetarea(p)
	{
	}
};

enum class RangeType
{
	MELEE,
	UNRANGED,
	RANGED,
	GLOBAL
};
struct Range
{
	constexpr Range(RangeType type, std::optional<size_t> range, std::optional<size_t> radius) :
	 type(type),
	 range(range),
	 radius(radius)
	{
	}
	RangeType type = RangeType::UNRANGED;
	std::optional<size_t> range = std::nullopt;
	std::optional<size_t> radius = std::nullopt;
};

class ActiveSkill
{ // use_skill(use_skill),
	public:
	ActiveSkill(void (*use_skill)(skill_arg arg)) : use_skill(use_skill) {}
	void (*use_skill)(skill_arg arg);
};

class PassiveSkill
{ // use_skill(use_skill),
	public:
	PassiveSkill(void (*on_enable)(), void (*on_disable)(), bool toogleable) :
	 on_enable(on_enable),
	 on_disable(on_disable),
	 toogleable(toogleable)
	{
	}
	void (*on_enable)();
	void (*on_disable)();
	bool toogleable;
};

class Skill
{
	private:
	constexpr Skill(const char* name,
	                const char* description,
	                Range range,
	                std::variant<ActiveSkill, PassiveSkill> type) :
	 name(name),
	 description(description),
	 range(range),
	 type(type)
	{
	}

	public:
	const char* name;
	const char* description;
	Range range;
	int level = 0;
	std::variant<ActiveSkill, PassiveSkill> type;

	static Skill Hit();
	static Skill Bash();
	static Skill Defend();
	static Skill Taunt();
	// all the skills as constructors
};

#endif // CPPRPG_CLASSES_SKILLS
