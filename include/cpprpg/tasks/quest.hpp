#ifndef CPPRPG_TASKS_QUEST_HPP
#define CPPRPG_TASKS_QUEST_HPP

#include <string>
#include <any>
#include <optional>
#include <set>
#include <cpprpg/common/config.hpp>

enum class QuestHooks {USE_SKILL};

struct QuestArg
{
	Character* self;
	Character* target;
	std::optional<Area> area;
	Skill* skill;
	std::optional<Point> location;
};

class Quest
{
	private:
		Quest(std::string name, std::set<QuestHooks> hooks, std::any meta, std::string (*get_description)(Quest& self), void(*update)(Quest& self, QuestArg arg), bool(*finished)(Quest& self)):
			name(name), hooks(hooks), meta(meta), get_description(get_description), update(update), finished(finished) {}
	public:
		std::string name;
		std::set<QuestHooks> hooks;
	    // contains quest-specific metadata
	    std::any meta;

	    std::string (*get_description)(Quest& self);
		void(*update)(Quest& self, QuestArg arg);
		bool(*finished)(Quest& self);

	    static Quest AttackSomething();
};

class QuestLog
{
	public:
		std::vector<Quest> quests;
		void add_quest(Quest(*quest_ctor)())
		{
			quests.push_back(quest_ctor());
		}

	    void update_quests(std::set<QuestHooks> hooks, QuestArg args)
		{
			for(auto quest : quests)
				for(auto hook : hooks)
					if(quest.hooks.count(hook))
						quest.update(quest, args);
		}
};

#endif // CPPRPG_TASKS_QUEST_HPP
