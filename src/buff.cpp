#include <cpprpg/characters/buff.hpp>
#include <cpprpg/characters/character.hpp>

Buff Buff::Poisoned()
{
	auto on_tick = [](Buff& self, Character& Char){UNUSED(self); Char.current_hp -= 1;};
	return Buff("Poisened", "You are poisened.", BuffType::BAD, 10, buff_nothing, on_tick, buff_nothing);
}
