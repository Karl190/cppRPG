#include <cpprpg/characters/character.hpp>
#include <cpprpg/characters/inventory.hpp>

unsigned Inventory::max_mass()
{
	return static_cast<unsigned>(owner.get_attributes().at(AttributeType::CONSTITUTION) * 200);
}
bool Inventory::equip_inv_item(size_t index, SlotType slot)
{
	const auto& equipped = owner.equipment.get_equip(slot);
	auto wearable = items[index];
	if(std::get<bool>(owner.equipment.get_tuple(slot)) && wearable.wearable &&
	   mass - wearable.mass + (equipped ? equipped->mass : 0) <= max_mass())
	{
		auto [result, tmp] = owner.equipment.equip(wearable, slot);
		if (result)
		{
			pull(index);
			for (auto item : tmp)
			{
				if (item)
				{
					Item i = *item;
					items.push_back(Item(i));
					mass += i.mass;
				}
			}
			return true;
		}
	}
	return false;
}
