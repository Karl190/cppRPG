#include <algorithm>
#include <cpprpg/characters/character.hpp>
#include <cpprpg/items/item.hpp>
bool Item::use_requirements_test(Character& user) const
{
	if(user.level >= lvl_requirement && user.get_attributes() >= attr_requirements &&
	   (!wearable || wearable->use_requirements_test(user)))
		return true;
	return false;
}
bool Wearable::use_requirements_test(Character& user) const
{
	if(user.character_class.weight == weight)
		return true;
	return false;
}
