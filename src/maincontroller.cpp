#define LEVEL_CAP 100

#include <cpprpg/characters/character.hpp>
#include <cpprpg/characters/player.hpp>
#include <cpprpg/common/config.hpp>
#include <cpprpg/common/gameobject.hpp>
#include <cpprpg/common/skilltreeparse.hpp>
#include <cpprpg/items/container.hpp>
#include <cpprpg/items/item.hpp>
#include <filesystem>
#include <iostream>
#include <vector>

int main()
{
	if (!config_folder_exists())
	{
		copy_default_configs();
	}
	load_default_configs();
	Player w("Elf", "Mage");
	// print_skilltree(skilltrees["Warrior"]);
	// w.character_class.wearable_types.insert(WearableType::AXE2H);
	// only works if CharacterClass is not const for testing only
	Item test(Attributes("Strohhut", {-5.4, 10, 10, 10, 10}),
	          100u,
	          Skill::Hit(),
	          100,
	          {},
	          Wearable(WearableType::HELMET, WearableWeight::HEAVY));
	Item test2(Attributes("Axter", {10.2, 50, 10, 10, 10}),
	           100u,
	           Skill::Hit(),
	           100,
	           {},
	           Wearable(WearableType::AXE2H, WearableWeight::HEAVY));
	std::cout << "davor" << std::endl;
	w.inventory.put(test);
	w.inventory.put(test2);
	w.level = 100;
	w.inventory.equip_inv_item(0, SlotType::HEAD);
	w.inventory.equip_inv_item(0, SlotType::MAINH);
	std::cout << "davor" << std::endl;
	w.level = 1;
	w.equipment.evaluate();
	w.equipment.evaluate();

	w.professions.insert(Profession("Hunter", w));
	std::cout << "da" << std::endl;
	Skill s = w.professions.find(Profession("Hunter", w))->use[0];
	std::cout << w.current_hp << std::endl;
	std::get<ActiveSkill>(s.type).use_skill(skill_arg(w, &w, Point(1, 1)));
	std::cout << w.current_hp << std::endl;
	Container c = Container(Point(2, 1));
	c.content.push_back(test2);
	std::cout << c.content.size() << std::endl;
	std::get<ActiveSkill>(s.type).use_skill(skill_arg(w, &c, Point(1, 1)));
	std::cout << c.content.size() << std::endl;
}
