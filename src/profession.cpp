#include <cpprpg/professions/profession.hpp>
#include <cpprpg/characters/character.hpp>

Profession::Profession(const std::string professionname, Character& character):
	character(character), attributes(professionname)
{
	if(!profession_conf.sections.count(professionname))
		throw std::runtime_error("Couldn't find " + professionname);
	auto section = profession_conf.sections[professionname];

	std::vector<std::string> attrs;
	attrs.push_back("constitution");
	attrs.push_back("strength");
	attrs.push_back("dexterity");
	attrs.push_back("intelligence");
	attrs.push_back("strength");
	attrs.push_back("initiative");

	for(auto& str : attrs)
	{
		if (section.count(str))
		{
			if(cconf::get_type(section[str]) != cconf_type::INT)
				throw std::runtime_error("Non-integer value as attribute for " + professionname);
			else
				attributes.at(AttributeType::CONSTITUTION) +=
				    cconf_get_value<cconf_int>(section[str]); // FIXME
		}
	}
	use.push_back(Skill::Hit());
}
