#include <cpprpg/tasks/quest.hpp>
#include <sstream>
#include <cpprpg/skills/skill.hpp>
#include <cstring>

using namespace std;

Quest Quest::AttackSomething()
{
	size_t left = 5;
	set<QuestHooks> hooks({QuestHooks::USE_SKILL});
	string name("Attack something");

	string (*get_description)(Quest& self) =
	[](Quest& self) -> std::string
	{
		stringstream ss;
		ss << "Attack " << any_cast<size_t>(self.meta) << " more people";
		return ss.str();
	};

	void(*update)(Quest& self, QuestArg arg) =
	[](Quest& self, QuestArg arg) -> void
	{
		if(strcmp(arg.skill->name, Skill::Hit().name) == 0)
			any_cast<size_t&>(self.meta)--;
	};

	bool(*finished)(Quest& self) =
	[](Quest& self) -> bool
	{
		return any_cast<size_t>(self.meta) == 0;
	};

	return Quest(name, hooks, left, get_description, update, finished);
}
