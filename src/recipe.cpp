#include <cpprpg/common/config.hpp>
#include <cpprpg/professions/recipe.hpp>

std::map<std::string, Recipe> init_recipes(std::string filename)
{
	cconf recipes(filename);
	std::map<std::string, Recipe> ret;
	
	for(auto [name, kvs] : recipes.sections)
	{
		Recipe& current = ret[name];
		for(auto [key, value] : kvs)
		{
			if(!std::holds_alternative<cconf_value>(value) ||
			   !std::holds_alternative<cconf_int>(std::get<cconf_value>(value)))
			{
				throw std::runtime_error("Non-integer value for " + key + " in " + RECIPES_FOLDER + "baseRecipes.cconf");
			} else {
				if(all_items.count(key))
					current.ingredients.push_back({all_items.find(key)->second, std::get<cconf_int>(std::get<cconf_value>(value))});
				else
					throw std::runtime_error("Unknown Item " + key);
			}
		}
	}
	
	return ret;
}
