#include <cpprpg/characters/character.hpp>
#include <cpprpg/items/container.hpp>
#include <cpprpg/skills/skill.hpp>
#include <map>

using namespace std;

Skill Skill::Hit()
{
	return Skill("Hit",
	             "Inflicts basic attack damage to an enemy",
	             Range(RangeType::MELEE, std::nullopt, std::nullopt),
	             ActiveSkill([](skill_arg arg) {
		             auto tmp = dynamic_cast<Character*>(arg.target);
		             if(tmp)
		             {
			             tmp->current_hp -= arg.user.get_attributes().at(AttributeType::STRENGTH);
		             }
		             else
		             {
			             auto tmp2 = dynamic_cast<Container*>(arg.target);
			             tmp2->content.clear();
		             }
	             }));
}

Skill Skill::Bash()
{
	return Skill("Bash",
	             "Inflicts 2 * basic attack damage to an enemy",
	             Range(RangeType::MELEE, std::nullopt, std::nullopt),
	             ActiveSkill(nullptr));
}

Skill Skill::Defend()
{
	return Skill("Defend",
	             "defends",
	             Range(RangeType::MELEE, std::nullopt, std::nullopt),
	             ActiveSkill(nullptr));
}

Skill Skill::Taunt()
{
	return Skill("Taunt",
	             "taunts",
	             Range(RangeType::MELEE, std::nullopt, std::nullopt),
	             ActiveSkill(nullptr));
}
